#include <sdktools_functions>
#include <csgo_colors>

#undef REQUIRE_PLUGIN
#include <sourcebans>

#undef REQUIRE_EXTENSIONS
#include <cstrike>
#include <SteamWorks>

stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_ANC.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)

#define VERSION "1.3.202"

public Plugin:myinfo =
{
	name		= "Auto Name Changer",
	author		= "Exle & ShaRen",
	description = "Replaces forbidden nicknames and clan tag",
	version		= VERSION,
	url			= "http://steamcommunity.com/id/ex1e/"
};

bool	g_bChanged[MAXPLAYERS+1];
bool	g_bSkipCheck[MAXPLAYERS+1];
Handle	NM_Enabled;
Handle	NM_ChangeName;
char	g_sNames[64][32];
int		g_iNames;
char	g_sClans[64][32];
int		g_iClans;

public void OnPluginStart()
{
	CreateConVar("ANC_Version", VERSION, "Auto Name Changer", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);

	NM_Enabled 				= CreateConVar("nm_enabled",			"1",		"1|0 Включить|Выключить плагин");
	NM_ChangeName			= CreateConVar("nm_changename",			"1",		"1|0 Включить|Выключить изменение имени игрока если он запрещен");

	HookEvent("player_changename", player_changename, EventHookMode_Pre);

	//UserMsg Message = GetUserMessageId("SayText2");
	//HookUserMessage(Message, SayText2, true);

	AutoExecConfig(true, "config.anc", "sourcemod/anc");
	CreateTimer(90.0, Message_Help, _, TIMER_REPEAT);

	Handle file = OpenFile("cfg/sourcemod/anc/names.ini", "rt");
	if (file != INVALID_HANDLE) {
		decl String:buffer[MAX_NAME_LENGTH];
		for (int i=0; !IsEndOfFile(file) && ReadFileLine(file, buffer, sizeof(buffer)); i++) {
			TrimString(buffer);
			Format(g_sNames[i], sizeof(g_sNames[]), buffer);
			g_iNames = i;
		}
		CloseHandle(file);
	} else dbgMsg("cfg/sourcemod/anc/names.ini file == INVALID_HANDLE");

	file = OpenFile("cfg/sourcemod/anc/clans.ini", "rt");
	if (file != INVALID_HANDLE) {
		decl String:buffer[MAX_NAME_LENGTH];
		for (int i=0; !IsEndOfFile(file) && ReadFileLine(file, buffer, sizeof(buffer)); i++) {
			TrimString(buffer);
			Format(g_sClans[i], sizeof(g_sClans[]), buffer);
			g_iClans = i;
		}
		CloseHandle(file);
	} else dbgMsg("cfg/sourcemod/anc/clans.ini file == INVALID_HANDLE");
}



public Action Message_Help(Handle timer)
{
	for (int i=1; i<GetMaxClients(); i++)
		if (IsClientConnected(i) && IsClientInGame(i) && !IsFakeClient(i) && g_bChanged[i]) {
			if (SteamWorks_HasLicenseForApp(i, 730) == k_EUserHasLicenseResultHasLicense) {
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}ВНИМАНИЕ!!! {BLUE}у вас стоит запрещенный ник");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}Пожалуйста поменяйте его на разрешенный.");
				CGOPrintToChat(i, "{GREEN}[S-IC] {BLUE}Если вы хотите оставить сайт в своем нике, то напишите его через пробел.");
				CGOPrintToChat(i, "{GREEN}[S-IC] {BLUE}Напрмиер: {RED}\"НИК servers-info.ru\"");
				CGOPrintToChat(i, "{GREEN}[S-IC] {BLUE}При заходе на наш сервер у вас удалится только сайт, а ник не изменится");
			} else {
				CGOPrintToChat(i, "{GREEN}[S-IC] {BLUE}Как же поменять ник в игре на пиратке?");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}Найдите в корне папки CS GO файл под кодовым именем {BLUE}rev.ini");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}Откройте его через любой текстовой редактор, кроме Word. Подойдёт обычный «Блокнот» / «NotePad»");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}Воспользуйтесь поиском путем комбинацией клавишей Ctrl+F и найти строку {BLUE}\"PlayerName =\"");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}После этого сохраните изменения в файле.");
				CGOPrintToChat(i, "{GREEN}[S-IC] {RED}Так же у вас может быть {BLUE}Ultimate Name Changer{RED} в папке с игрой.");
			}
		}
	return Plugin_Continue;
}

//public void OnMapStart()
//{
//	if (!DirExists("cfg/sourcemod/anc/"))
//		CreateDirectory("cfg/sourcemod/anc/", 511);
//	if (!FileExists(file_names)) {
//		new Handle:file = OpenFile(file_names, "wt");
//		WriteFileLine(file, ".ua\n.uа\n.ru\n.ro\n.ro\n.com\n.сom\n.cоm\n.соm\n.info\n.infо\nYour name\nYou name\nPlayer\n.su\n.net\n.nеt\n.lt");
//		CloseHandle(file);
//	}
//	if (!FileExists(file_clans)) {
//		new Handle:file = OpenFile(file_clans, "wt");
//		WriteFileLine(file, ".ua\n.uа\n.ru\n.ro\n.ro\n.com\n.сom\n.cоm\n.соm\n.info\n.infо\n.su\n.net\n.nеt\n.lt");
//		CloseHandle(file);
//	}
//}

public void OnClientPostAdminCheck(int client)
{
	//dbgMsg("OnClientPostAdminCheck %N", client);
	g_bChanged[client] = false;
	if (!GetConVarBool(NM_Enabled) || !GetConVarBool(NM_ChangeName) || IsFakeClient(client))
		return;
	decl String:client_name[MAX_NAME_LENGTH];
	GetClientName(client, client_name, sizeof(client_name));
	if (Check(true, client_name) != -1) {
		//dbgMsg("Check newname");
		ChangeName(client);
		return;
	}
	int iD = Check(false, client_name);
	if (iD > 4)
		TrimName(client, iD, client_name);
	return;
}

public Action player_changename(Event e, const char[] name, bool dontBroadcast)
{
	//dbgMsg("player_changename");
	int client = GetClientOfUserId(e.GetInt("userid"));
	if(g_bSkipCheck[client]) {				// значит смена ника вызвана плагином
		g_bSkipCheck[client] = false;		// пропускаем чтобы снять нагрузку
		//dbgMsg("g_bSkipCheck");
		return Plugin_Continue;
	}
	if (!GetConVarBool(NM_Enabled) || !GetConVarBool(NM_ChangeName) || IsFakeClient(client)) 
		return Plugin_Continue;

	decl String:newname[MAX_NAME_LENGTH], 
		 String:oldname[MAX_NAME_LENGTH];
	newname[0] = '\0',
	oldname[0] = '\0';

	e.GetString("newname", newname, sizeof(newname));
	e.GetString("oldname", oldname, sizeof(oldname));
	//dbgMsg("player_changename new\"%s\" old\"%s\"", newname, oldname);
	if (Check(true, newname) != -1) {
		dbgMsg("player_changename file_names Plugin_Handled  new\"%s\" old\"%s\"", newname, oldname);
		//e.SetString("newname", oldname);

		g_bSkipCheck[client] = true;
		SetClientName(client, oldname);
		return Plugin_Handled;
	}
	if (Check(false, newname) != -1) {
		dbgMsg("player_changename file_clans Plugin_Handled  new\"%s\" old\"%s\"", newname, oldname);
		//e.SetString("newname", oldname);
		g_bSkipCheck[client] = true;
		SetClientName(client, oldname);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

int Check(bool bName, char[] name)
{
	if (bName) {
		for(int i=0; i<=g_iNames; i++) {
			int iC = StrContains(name, g_sNames[i], false);
			if (iC != -1) {
				if (iC)
					dbgMsg("Check %s in %s (%i)", g_sNames[i], name, iC);
				return iC;
			}
		}
	} else {
		for(int i=0; i<=g_iClans; i++) {
			int iC = StrContains(name, g_sClans[i], false);
			if (iC != -1) {
				if (iC)
					dbgMsg("Check %s in %s (%i)", g_sClans[i], name, iC);
				return iC;
			}
		}
	}
	return -1;
}

void ChangeName(int client)
{
	//dbgMsg("ChangeName");
	char new_name[MAX_NAME_LENGTH], auth[15];
	//new_name[0] = '\0';
	if (!GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth)))
		Format(new_name, sizeof(new_name), "Заключенный");
	else Format(new_name, sizeof(new_name), "Заключенный %s", auth[10]);
	g_bChanged[client] = true;
	g_bSkipCheck[client] = true;
	SetClientName(client, new_name);
	
	//dbgMsg("%s поменял ник на %s", auth, new_name);
	PrintToChat(client, "\x04[ANC]\x01 Ваш ник запрещен, он изменен на %s", new_name);
	return;
}

void TrimName(int client, int iD, char[] name)
{
	dbgMsg("TrimName");
	for(iD; iD>0; --iD) {
		if (name[iD] == ' ') {
			Format(name, iD+1, "%s", name);
			if (Check(false, name) == -1) {
				dbgMsg("%N меняет ник на %s", client, name);
				break;
			}
		}
		if (iD == 2) {			// если имя до сайта меньше 2 символов
			dbgMsg("имя до сайта меньше 2 символов");
			ChangeName(client);
			return;
		}
	}
	g_bSkipCheck[client] = true;
	SetClientName(client, name);
	PrintToConsole(client, "[ANC] Ваш ник запрещён, он изменён на %s", name);
	return;
}

//public Action SayText2(UserMsg msg_id, Handle bf, const int[] players, int playersNum, bool reliable, bool init)
//{
//	dbgMsg("SayText2");
//	if (!GetConVarBool(NM_Enabled) || !GetConVarBool(NM_ChangeName))
//		return Plugin_Continue;
//	decl String:message[256],
//		String:oldname[MAX_NAME_LENGTH],
//		String:newname[MAX_NAME_LENGTH];
//
//	if (GetUserMessageType() == UM_BitBuf) {
//		BfReadString(bf, message, sizeof(message));
//		dbgMsg(" == UM_BitBuf message1 %s ", message);
//		BfReadString(bf, message, sizeof(message));
//		BfReadString(bf, oldname, sizeof(oldname));
//		BfReadString(bf, newname, sizeof(newname));
//		dbgMsg(" == UM_BitBuf message %s oldname  %s newname  %s", message, oldname, newname);
//	} else {
//		PbReadString(bf, "params", message, sizeof(message), 1);
//		PbReadString(bf, "params", oldname, sizeof(oldname), 2);
//		PbReadString(bf, "params", newname, sizeof(newname), 3);
//		dbgMsg(" != UM_BitBuf message %s oldname  %s newname  %s", message, oldname, newname);
//	}
//	
//
//	return (StrContains(message, "#Cstrike_Name_Change") != -1 && (Check(true, message)!= -1 || Check(false, message)!= -1 )  ) ? Plugin_Handled : Plugin_Continue;
//}